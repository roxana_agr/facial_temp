#!/usr/bin/env python
"""A script to define the regions of interest on a face."""
# v0.1 03/01/2018
# definition of ROIS

import os
from regions_of_interest import RegionsOfInterest
from utils import generate_writer


class DataFiles(object):
    """Class for regions of interest."""

    def __init__(self, regions, coords_x, coords_y, params):
        """Initialization method."""
        self.regions = regions
        self.coords_x = coords_x
        self.coords_y = coords_y
        self.rois = RegionsOfInterest(coords_x, coords_y)
        self.parameters = params

    def create_data_files(self):
        """Method which checks how many regions are required."""
        if len(self.regions) == 1:
            if self.regions[0] == 'all':
                coord = self.rois.get_all_regions()
                coords = coord.items()
            elif self.regions[0] == 'noface':
                coord = self.rois.get_no_face()
                coords = coord.items()
            else:
                coordis = self.rois.get_one_region(self.regions[0])
                print coordis
                coords = [(self.regions[0], coordis)]
        else:
            coords = self.rois.get_multiple_regions(self.regions)
        return coords

    def generate_file_name(self, regname):
        """Method which generates the name of the output file."""
        save_path = self.parameters[0]
        participant = self.parameters[1]
        condition = self.parameters[2]
        argument = str(self.parameters[3])
        sep = "_"
        print condition, argument, regname
        file_extension = sep.join([condition, argument, regname])
        final_extension = file_extension + '.csv'
        file_name = os.path.join(save_path, participant,
                                 final_extension)
        return file_name

    def associate_files_coordinates(self):
        """Method which associates a region with its corresponding file.

        It also returns the coordinates of that region.
        """
        coordinates = self.create_data_files()
        final_data = []
        for reg in coordinates:
            filename = self.generate_file_name(reg[0])
            reg_writer = generate_writer(filename)
            if self.parameters[4]:
                reg_writer.writerow(['Time', 'Region', 'Temp', 'Count', 'Average'])
            final = (reg[0], filename, reg[1], reg_writer)
            final_data.append(final)
        return final_data
