#!/usr/bin/env python
"""Script to create the folder for the participant."""
import os
import sys

PATH = sys.argv[1]
PARTICIPANT = sys.argv[2]

PARTICIPANTS = os.listdir(PATH)
if PARTICIPANT in PARTICIPANTS:
    print 'I already have this participant'
else:
    PARTICIPANT_PATH = os.path.join(PATH, PARTICIPANT)
    os.mkdir(PARTICIPANT_PATH)
