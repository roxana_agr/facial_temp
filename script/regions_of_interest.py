#!/usr/bin/env python
"""A script to define the regions of interest on a face."""
# v0.1 03/01/2018
# definition of ROIS


class RegionsOfInterest(object):
    """Class for regions of interest."""

    def __init__(self, coords_x, coords_y):
        """Initialization method."""
        self.coords_x = coords_x
        self.coords_y = coords_y

        self.eyes_dist = self.coords_x[4] - self.coords_x[3]
        self.small_side = self.eyes_dist / 3
        self.half_small = self.small_side / 2
        self.medium_side = self.eyes_dist / 2

        self.regions = {'face': self.define_entire_face(),
                        'forehead': self.define_forehead(),
                        'left_cheek': self.define_left_cheek(),
                        'right_cheek': self.define_right_cheek(),
                        'left_periorbital': self.define_left_periorbital(),
                        'right_periorbital': self.define_right_periorbital(),
                        'chin': self.define_chin(),
                        'perinasal': self.define_perinasal(),
                        'nose': self.define_nose()}
        self.no_face_regions = {'forehead': self.define_forehead(),
                                'left_cheek': self.define_left_cheek(),
                                'right_cheek': self.define_right_cheek(),
                                'left_periorbital': self.define_left_periorbital(),
                                'right_periorbital': self.define_right_periorbital(),
                                'chin': self.define_chin(),
                                'perinasal': self.define_perinasal(),
                                'nose': self.define_nose()}

    def define_forehead(self):
        """Method which returns the coordinates of the forehead region."""
        interm_point = self.coords_y[6] - self.coords_y[3]
        coord_x = self.coords_x[0]
        coord_y = self.coords_y[0] - self.half_small
        coord_x1 = self.coords_x[1]
        coord_x2 = self.coords_y[1] - interm_point

        forehead = [coord_x, coord_y, coord_x1, coord_x2]
        return forehead

    def define_left_periorbital(self):
        """Method which returns the coordinates of the periorbital region."""
        coord_x = self.coords_x[4] - self.half_small
        coord_y = self.coords_y[4] - self.half_small
        coord_x1 = self.coords_x[4] + self.half_small
        coord_y1 = self.coords_y[4] + self.half_small

        left_periorbital = [coord_x, coord_y, coord_x1, coord_y1]
        return left_periorbital

    def define_right_periorbital(self):
        """Method which returns the coordinates of the periorbital region."""
        coord_x = self.coords_x[3] - self.half_small
        coord_y = self.coords_y[3] - self.half_small
        coord_x1 = self.coords_x[3] + self.half_small
        coord_y1 = self.coords_y[3] + self.half_small

        right_periorbital = [coord_x, coord_y, coord_x1, coord_y1]
        return right_periorbital

    def define_right_cheek(self):
        """Method which returns the coordinates of the cheek region."""
        interm_point1 = int((self.coords_y[7] - self.coords_y[2]) / 2)
        interm_point2 = int((self.coords_y[9] - self.coords_y[7]) / 2)
        coord_x = self.coords_x[2]
        coord_y = int(self.coords_y[7]) - interm_point1
        coord_x1 = self.coords_x[3]
        coord_y1 = int(self.coords_y[9]) - interm_point2

        right_cheek = [coord_x, coord_y, coord_x1, coord_y1]
        return right_cheek

    def define_left_cheek(self):
        """Method which returns the coordinates of the cheek region."""
        interm_point1 = int((self.coords_y[7] - self.coords_y[5]) / 2)
        interm_point2 = int((self.coords_y[10] - self.coords_y[7]) / 2)
        coord_x = self.coords_x[4]
        coord_y = int(self.coords_y[7]) - interm_point1
        coord_x1 = self.coords_x[5]
        coord_y1 = int(self.coords_y[10]) - interm_point2

        left_cheek = [coord_x, coord_y, coord_x1, coord_y1]
        return left_cheek

    def define_chin(self):
        """Method which returns the coordinates of the chin region."""
        coord_x = self.coords_x[9]
        coord_y = self.coords_y[9] + self.small_side
        coord_x1 = self.coords_x[10]
        coord_y1 = self.coords_y[9] + self.eyes_dist

        chin = [coord_x, coord_y, coord_x1, coord_y1]
        return chin

    def define_nose(self):
        """Method which returns the coordinates of the nose region."""
        coord_x = self.coords_x[7] - self.half_small
        coord_y = self.coords_y[7] - self.half_small
        coord_x1 = self.coords_x[7] + self.half_small
        coord_y1 = self.coords_y[7] + self.half_small

        nose = [coord_x, coord_y, coord_x1, coord_y1]
        return nose

    def define_perinasal(self):
        """Method which returns the coordinates of the perinasal region."""
        coord_x = self.coords_x[6]
        coord_y = self.coords_y[7] # - self.small_side
        coord_x1 = self.coords_x[10]
        coord_y1 = self.coords_y[10]

        perinasal = [coord_x, coord_y, coord_x1, coord_y1]
        return perinasal

    def define_entire_face(self):
        """Method which returns the coordinates of the face region."""
        coord_x = self.coords_x[2]
        coord_y = self.coords_y[0] - self.eyes_dist
        coord_x1 = self.coords_x[5]
        coord_y1 = self.coords_y[10] + self.eyes_dist

        face = [coord_x, coord_y, coord_x1, coord_y1]
        return face

    def get_all_regions(self):
        """Method which returns the coordinates of all ROIs."""
        return self.regions

    def get_no_face(self):
        """Method which returns the coordinates of all ROIs except the face."""
        return self.no_face_regions

    def get_one_region(self, region):
        """Method which returns the coordinates of only one ROI."""
        return self.regions[region]

    def get_multiple_regions(self, regions):
        """Method which returns the coordinates of a selection of ROIs."""
        # regions is a list with all the regions to be extracted
        region = [] * len(regions)
        for index, reg in enumerate(regions):
            region[index] = self.regions[reg]
        return region
