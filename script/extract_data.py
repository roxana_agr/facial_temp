#!/usr/bin/env python
"""Script extracting the physiological parameters from the rgb data."""
# v0.1 16/08/2017
# created the first version of this script to
# extract the physiological parameters


import sys
import time
import numpy as np
from sensor_msgs.msg import Image
import cv2
from cv_bridge import CvBridge, CvBridgeError
import dlib
import rospkg
import rospy
from utils import clean, conversie
from collections import Counter
from data_files import DataFiles


class ProcessSensors(object):
    """Class for processing the sensors."""

    def __init__(self):
        """Initialization method."""
        # self.frame = None
        # self.data = None

        self.ros_data = {'time': rospy.get_param('/get_data/time_redo'),
                         'visual': rospy.get_param('/get_data/visualize'),
                         'ros': rospkg.RosPack(),
                         'tview': rospy.Subscriber('/optris/thermal_image_view',
                                                   Image,
                                                   self.processing),
                         'traw': rospy.Subscriber('/optris/thermal_image',
                                                  Image,
                                                  self.process_thermal)}
        self.params = {'bridge': CvBridge(),
                       'dets': [],
                       'tracker': dlib.correlation_tracker(),
                       'path': "/data/predictor_chuang.dat",
                       'counter': 0,
                       'start': time.time(),
                       'frame': None,
                       'data': None,
                       'temp': None,
                       'timer': None,
                       'times': None,
                       'write_counter': 0,
                       'first_open': False,
                       'landmarks_x': [],
                       'landmarks_y': [],
                       'regions': rospy.get_param('/get_data/regions')}

        predictor_path = self.ros_data['ros'].get_path("facial_temp")
        predict = predictor_path + self.params['path']
        self.predictor = dlib.shape_predictor(predict)
        self.detector = dlib.simple_object_detector(
            predictor_path + "/data/detector_chuang.svm")

        self.pos = None

    def processing(self, data):
        """Main processing method."""
        try:
            self.params['frame'] = self.params['bridge'].imgmsg_to_cv2(
                data, 'bgr8')
            # self.params['times'] = data.header.stamp
        except CvBridgeError as err:
            print err

        # self.process_thermal()
        # self.show()
        # do the face detection and tracking
        if len(self.params['dets']) == 0:
            self.params['counter'] = 0
            self.params['dets'] = self.detector(self.params['frame'], 1)
        else:
            self.params['write_counter'] += 1
            self.pos = clean(self.params['dets'][0])
            self.params['counter'] = self.params['counter'] + 1
            if self.params['counter'] == 1:
                self.params['tracker'].start_track(self.params['frame'],
                                                   dlib.rectangle(self.pos[0],
                                                                  self.pos[1],
                                                                  self.pos[2],
                                                                  self.pos[3]))
            else:
                self.params['tracker'].update(self.params['frame'])
                posi = self.params['tracker'].get_position()
                self.pos = clean(posi)

            # get the features
            self.get_features()
            self.get_regions()

        if time.time() - self.params['start'] > self.ros_data['time']:
            self.params['start'] = time.time()
            rospy.loginfo('Redo detection')
            self.params['dets'] = []
        self.show()

    def get_regions(self):
        """Here I extract the regions."""
        # print (self.params['regions'])
        if self.params['write_counter'] == 1:
            self.params['first_open'] = True
        else:
            self.params['first_open'] = False
        parameters = [sys.argv[1], sys.argv[2],
                      sys.argv[3], sys.argv[4],
                      self.params['first_open']]
        new_regs = self.params['regions'].split(",")

        files = DataFiles(new_regs,
                          self.params['landmarks_x'],
                          self.params['landmarks_y'],
                          parameters)
        my_regions_coordinates = files.associate_files_coordinates()
        for region in my_regions_coordinates:
            self.region(region)
        # self.region(my_regions_coordinates[1][2])

    def region(self, region_data):
        """Method to extract the temperature from a region and write it."""
        # in opencv x reprezinta coloana si y reprezinta linia
        # la temperature ar trebui sa iau y:y1 - x:x1
        points = region_data[2]
        coordx = int(points[0])
        coordy = int(points[1])
        coordx1 = int(points[2])
        coordy1 = int(points[3])

        region = [coordx, coordy, coordx1, coordy1]
        self.loc_face(region)
        if coordy < coordy1:
            temperature = self.params['temp'][coordy:coordy1, coordx:coordx1]
        else:
            temperature = self.params['temp'][coordy1:coordy, coordx:coordx1]
        temperature = temperature.flatten()
        average = np.mean(temperature)
        temp = Counter(temperature).keys()
        count = Counter(temperature).values()
        # self.params['timer'] = self.params['times']

        region_data[3].writerow([self.params['timer'],
                                 region_data[0],
                                 temp,
                                 count,
                                 average])
        # if val == 1:
        #     self.writer['wr1'].writerow([self.params['timer'], average])
        # elif val == 2:
        #     self.writer['wr2'].writerow([self.params['timer'], average])
        # elif val == 3:
        #     self.writer['wr3'].writerow([self.params['timer'], average])
        # elif val == 4:
        #     self.writer['wr4'].writerow([self.params['timer'], average])
        # elif val == 5:
        #     self.writer['wr5'].writerow([self.params['timer'], average])
        # elif val == 6:
        #     self.writer['wr6'].writerow([self.params['timer'], average])

    def show(self):
        """Method to visualize data."""
        if self.ros_data['visual']:
            # print self.pos
            cv2.rectangle(self.params['frame'], (self.pos[0],
                                                 self.pos[1]),
                          (self.pos[2],
                           self.pos[3]),
                          (255, 255, 0), 1)

            cv2.imshow('Window', self.params['frame'])
            cv2.waitKey(3)

    def loc_face(self, data):
        """Put rectangle around the face."""
        cv2.rectangle(self.params['frame'],
                      (int(data[0]), int(data[1])),
                      (int(data[2]), int(data[3])),
                      (255, 255, 0), 1)

    def get_features(self):
        """Method to get the features."""
        # extract the features
        self.params['landmarks_x'] = []
        self.params['landmarks_y'] = []
        posi = self.pos
        # print posi
        shapes = self.predictor(self.params['frame'],
                                dlib.rectangle(long(posi[0]),
                                               long(posi[1]),
                                               long(posi[2]),
                                               long(posi[3])))
        for fpoint in shapes.parts():
            self.params['landmarks_x'].append(float(fpoint.x))
            self.params['landmarks_y'].append(float(fpoint.y))
            # cv2.circle(self.params['frame'], (fpoint.x, fpoint.y),
            #            1, (255, 0, 0), 2)

    def process_thermal(self, data):
        """Method to have the raw thermal data in degrees."""
        new_temp = conversie(data.data)
        new_temp = np.reshape(new_temp, (np.shape(self.params['frame'])[0],
                                         np.shape(self.params['frame'])[1]))
        self.params['timer'] = data.header.stamp
        self.params['temp'] = (new_temp - 1000) / 10.0


def main(args):
    """Main function."""
    rospy.init_node('process', anonymous=True)
    ProcessSensors()
    try:
        rospy.spin()
    except KeyboardInterrupt:
        print 'Shutting down'

if __name__ == '__main__':
    main(sys.argv)
