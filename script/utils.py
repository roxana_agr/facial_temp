#!/usr/bin/env python
"""Script with functions used in the package."""
import numpy as np

from scipy.signal import butter, lfilter
import struct
import csv


def clean(data):
    """Clean the coordinates of the face."""
    # (0,0) is in the top left corner
    new = str(data)
    new_data = new.split(" ")
    xleft = int(float((new_data[0][2:-1])))
    yup = int(float((new_data[1][:-1])))
    xright = int(float((new_data[2][1:-1])))
    ydown = int(float((new_data[3][:-2])))

    return [xleft, yup, xright, ydown]


def smooth_triangle(data, degree, drop_values=False):
    """Function to smooth the input data."""
    # degree could be 3/5/10
    triangle = np.array(range(degree) + [degree] + range(degree)[::-1]) + 1
    smoothed = []
    for i in range(degree, len(data) - degree * 2):
        point = data[i:i + len(triangle)] * triangle
        smoothed.append(sum(point) / sum(triangle))
    if drop_values:
        return smoothed
    smoothed = [smoothed[0]] * (degree + degree / 2) + smoothed
    while len(smoothed) < len(data):
        smoothed.append(smoothed[-1])
    return smoothed


def butter_lowpass_filter(data, cutoff, sampling, order=5):
    """Lowpass filter."""
    coeffb, coeffa = butter_lowpass(cutoff, sampling, order=order)
    outputy = lfilter(coeffb, coeffa, data)
    return outputy


def butter_lowpass(cutoff, sampling, order=5):
    """Gfafda."""
    nyq = 0.5 * sampling
    normal_cutoff = cutoff / nyq
    coeffb, coeffa = butter(order, normal_cutoff, btype='low', analog=False)
    return coeffb, coeffa


def ffttransform(signal, signal_duration):
    """Method for computing the fft."""
    # Fft inital computations
    signal_len = len(signal)
    vart = float(signal_duration) / signal_len
    # Compute the Fourier Transform
    frequency = np.linspace(0.0, 1.0 / (2.0 * vart), signal_len / 2)
    magnitude = np.abs(np.fft.fft(signal))
    # shiftFq = np.fft.fftshift(magnitude)
    return frequency, magnitude, signal_len


def butter_bandpass(lowcut, highcut, sampling, order=3):
    """Butterworth bandpass filter."""
    nyq = 0.5 * sampling
    low = lowcut / nyq
    high = highcut / nyq
    coeffb, coeffa = butter(order, [low, high], btype='band')
    return coeffb, coeffa


def butterbandpass_filter(data, lowcut, highcut, sampling, order=3):
    """Standard  ButterW bpf."""
    coeffb, coeffa = butter_bandpass(lowcut, highcut, sampling, order=order)
    outputy = lfilter(coeffb, coeffa, data)
    return outputy


def channel_extractor(frame, rgb):
    """Extract mean value from rgb channel."""
    channel_r = frame[:, :, 0]
    channel_g = frame[:, :, 1]
    channel_b = frame[:, :, 2]

    if rgb is 'r':
        r_mean = np.mean(channel_r)
        return float(r_mean)

    if rgb is 'g':
        g_mean = np.mean(channel_g)
        return float(g_mean)

    if rgb is 'b':
        b_mean = np.mean(channel_b)
        return float(b_mean)


def conversie(date):
    """Function to convert the thermal data."""
    data = []
    for i in range(0, len(date), 2):
        data.append(struct.unpack("<H", date[i:i + 2]))
    return np.array(data)


def generate_writer( filename):
    """Method to generate the writer and return it."""
    files = open(filename, 'a')
    writer = csv.writer(files)
    return writer
